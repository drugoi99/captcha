package captcha

import (
	"image/color"
	"net/http"

	"github.com/gorilla/sessions"
	"github.com/steambap/captcha"
	"golang.org/x/image/font/gofont/gobold"
)

var (
	salt = []byte("Ws3VFp#a84x")
	//store = map[string]*Captcha{}
	alpha              = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	store              = sessions.NewCookieStore(salt)
	sessionStorageName = "cpt_session"

	ImgWidth    = 110
	ImgHeight   = 40
	TextLength  = 4
	CurveNumber = 6
	FontDPI     = 60.0
	Noise       = 3.0
	//BackgroundColor = color.Transparent
	BackgroundColor = color.RGBA{255, 255, 255, 255}
	CharPreset      = alpha
)

func GetImage(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, sessionStorageName)
	captcha.LoadFont(gobold.TTF)
	data, _ := captcha.New(ImgWidth, ImgHeight, func(options *captcha.Options) {
		options.BackgroundColor = BackgroundColor
		options.CharPreset = CharPreset
		options.CurveNumber = CurveNumber
		options.FontDPI = FontDPI
		options.Noise = Noise
		options.TextLength = TextLength
	})
	session.Values["captcha"] = data.Text
	session.Save(r, w)
	data.WriteImage(w)
}

func IsValid(r *http.Request, postedCode string) bool {
	session, _ := store.Get(r, sessionStorageName)
	if postedCode == session.Values["captcha"].(string) {
		return true
	}
	return false
}
